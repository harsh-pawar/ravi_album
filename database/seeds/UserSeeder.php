<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $users =
            array(
                array('email' => 'ravi.jakasaniya.1@gmail.com', 'password'=>Hash::make("Rav@123919"), 'name'=> 'Ravi'),
            );

        DB::table('users')->insert($users);
    }
}
