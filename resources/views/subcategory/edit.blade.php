@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Album Sub Category</div>
<?php $i=0; ?>
                    <form method="post" action="/subcategory/{{$subcategory->id}}/update">
                        @csrf
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Album Category</label>

                                <div class="col-md-8">
                                    <input id="category" type="text" class="form-control" name="category_name"
                                           value="{{ $subcategory->category->category_name }}"
                                           required disabled>
                                </div>
                            </div>
                                <div id="sub-category-div">
                                    <div class="form-group row" id="sub_category_{{ $i }}">

                                        <label for="title_{{$i}}" class="col-md-2 col-form-label text-md-right">SubCategory Title</label>

                                        <div class="col-md-3">
                                            <input id="title_{{$i}}" type="text" value="{{ $subcategory->title }}" class="form-control" name="title" required autofocus>
                                        </div>
                                        <label for="subtitle_{{$i}}" class="col-md-2 col-form-label text-md-right">SubCategory SubTitle</label>

                                        <div class="col-md-3">
                                            <input id="subtitle_{{$i}}" type="text" value="{{ $subcategory->subtitle }}" class="form-control" name="subtitle" required autofocus>
                                        </div>
                                        {{--<div class="col-md-2">
                                            <button class="btn btn-primary plus_btn" id="plus_{{ $i }}" onclick="addSubCategory({{ $i }})">
                                                +
                                            </button>
                                            <button class="btn btn-danger remove_btn hide" onclick="removeSubCategory({{ $i }})">
                                                -
                                            </button>
                                        </div>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-2 col-form-label text-md-right">Status</label>

                                    <div class="col-md-3" style="margin-top: 5px;">
                                        <input type="radio" id="active" name="status" value="active"
                                               @if($subcategory->status == 'active')checked @endif>
                                        <label for="active">Active</label>
                                        <input type="radio" id="inactive" name="status" value="inactive"
                                               @if($subcategory->status == 'inactive')checked @endif>
                                        <label for="inactive">InActive</label>
                                    </div>
                                    <label for="email" class="col-md-2 col-form-label text-md-right">Sub Priority</label>

                                    <div class="col-md-3">
                                        <input id="sub_priority" type="number" class="form-control" name="sub_priority"
                                               value="{{ $subcategory->sub_priority }}"
                                               required>
                                    </div>
                                </div>


                            <div class="form-group row">
                                <div class="col-md-10" align="right">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection