@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Album Sub Category</div>
                    <?php $i=0; ?>
                    <form method="post" action="/subcategory">
                        @csrf
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Album Category</label>

                                <div class="col-md-8">
                                    <select id="category" type="text" class="form-control" name="album_category_id" required>
                                        <option value="">Select Category</option>
                                        @foreach($categories as $id=>$category)
                                            <option value="{{ $id }}">{{ $category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="sub-category-div">
                                <div class="form-group row" id="sub_category_{{ $i }}">

                                    <label for="title_{{$i}}" class="col-md-2 col-form-label text-md-right">SubCategory Title</label>

                                    <div class="col-md-3">
                                        <input id="title_{{$i}}" type="text" class="form-control" name="title" required autofocus>
                                    </div>
                                    <label for="subtitle_{{$i}}" class="col-md-2 col-form-label text-md-right">SubCategory SubTitle</label>

                                    <div class="col-md-3">
                                        <input id="subtitle_{{$i}}" type="text" class="form-control" name="subtitle" required autofocus>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Status</label>

                                <div class="col-md-3" style="margin-top: 5px;">
                                    <input type="radio" id="active" name="status" value="active" checked>
                                    <label for="active">Active</label>
                                    <input type="radio" id="inactive" name="status" value="inactive">
                                    <label for="inactive">InActive</label>
                                </div>
                                <label for="email" class="col-md-2 col-form-label text-md-right">Sub Priority</label>

                                <div class="col-md-3">
                                    <input id="sub_priority" type="number" class="form-control" name="sub_priority"
                                           required>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-10" align="right">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection