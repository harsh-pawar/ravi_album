@extends('layouts.app')

@section('content')
    <?php $i = 0; ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Album Category</div>
                    <table width="100%" border="1">
                        <thead align="center">
                            <tr>
                                <th>Category Name</th>
                                <th>Title</th>
                                <th>SubTitle</th>
                                <th>Status</th>
                                <th>SubPriority</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                        @foreach($title_array as $id=>$title)
                            <tr>
                                <td>{{ isset($title->category) ? $title->category->category_name : "NA" }}</td>
                                <td>{{ $title->title }}</td>
                                <td>{{ $title->subtitle }}</td>
                                <td>{{ $title->status == 'active' ? "Active" : "Inactive" }}</td>
                                <td>{{ $title->sub_priority }}</td>
                                <td>
                                    <a href="/subcategory/{{$title->id}}/edit">Edit</a> |
                                    <a href="/subcategory/{{$title->id}}/delete">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <style>
        .hide {
            visibility: hidden;
        }
    </style>
@endsection

@section('js-script')
    <script>

    </script>
@endsection
