@extends('layouts.app')

@section('content')
    <?php $i = 0; ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Album Category</div>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table width="100%" border="1">
                        <thead align="center">
                            <tr>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Priority</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                            @foreach($title_array as $id=>$title)
                            <tr>
                                <td>{{ $title->category_name }}</td>
                                <td>{{ $title->status == 'active' ? "Active" : "Inactive" }}</td>
                                <td>{{ $title->priority }}</td>
                                <td>
                                    <a href="/category/{{$title->id}}/edit">Edit</a> |
                                    <a href="#" onclick="deleteCategory({{$title->id}})">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <style>
        .hide {
            visibility: hidden;
        }
    </style>
@endsection

@section('js-script')
    <script>
        function deleteCategory(id) {
            if (confirm('Are you sure ?')) {
                window.location.replace("/category/"+id+"/delete");
            }
        }
    </script>
@endsection
