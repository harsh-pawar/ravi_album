@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Album Category</div>

                    <form method="post" action="/category/{{$category->id}}/update">
                        @csrf
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Album Category</label>

                                <div class="col-md-8">
                                    <input id="category" type="text" class="form-control" name="category_name"
                                           value="{{ $category->category_name }}"
                                           required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Priority</label>

                                <div class="col-md-8">
                                    <input id="category" type="number" class="form-control" name="priority"
                                           value="{{ $category->priority }}"
                                           required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Status</label>

                                <div class="col-md-6" style="margin-top: 5px;">
                                    <input type="radio" id="active" name="status" value="active"
                                           @if($category->status == 'active')checked @endif>
                                    <label for="active">Active</label>
                                    <input type="radio" id="inactive" name="status" value="inactive"
                                           @if($category->status == 'inactive') checked @endif>
                                    <label for="inactive">InActive</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-10" align="right">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection