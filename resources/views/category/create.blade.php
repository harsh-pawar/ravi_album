@extends('layouts.app')

@section('content')
    <?php $i = 0; ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Album Category</div>

                    <form method="post" action="/category">
                        @csrf
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Album Category</label>

                                <div class="col-md-8">
                                    <input id="category_{{ $i }}" type="text" class="form-control" name="category_name" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Status</label>

                                <div class="col-md-3" style="margin-top: 5px;">
                                    <input type="radio" id="active" name="status" value="active" checked>
                                    <label for="active">Active</label>
                                    <input type="radio" id="inactive" name="status" value="inactive">
                                    <label for="inactive">InActive</label>
                                </div>

                                <label for="email" class="col-md-2 col-form-label text-md-right">Priority</label>

                                <div class="col-md-3">
                                    <input type="number" class="form-control" name="priority" required>
                                </div>
                            </div>
                            <div id="sub-category-div">
                                <div class="form-group row" id="sub_category_{{ $i }}">

                                    <label for="title_{{$i}}" class="col-md-2 col-form-label text-md-right">SubCategory Title</label>

                                    <div class="col-md-3">
                                        <input id="title_{{$i}}" type="text" class="form-control" name="title[]" required autofocus>
                                    </div>
                                    <label for="subtitle_{{$i}}" class="col-md-2 col-form-label text-md-right">SubCategory SubTitle</label>

                                    <div class="col-md-3">
                                        <input id="subtitle_{{$i}}" type="text" class="form-control" name="subtitle[]" required autofocus>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary plus_btn" id="plus_{{ $i }}" onclick="addSubCategory({{ $i }})">
                                            +
                                        </button>
                                        <button class="btn btn-danger remove_btn hide" onclick="removeSubCategory({{ $i }})">
                                            -
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-10" align="right">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <style>
        .hide {
            visibility: hidden;
        }
    </style>
@endsection

@section('js-script')
    <script>
        function removeSubCategory(i) {
            $("#sub_category_"+i).remove();
            $( ".plus_btn" ).last().removeClass( "hide" );
            if($( ".remove_btn" ).length == 1) {
                $(".remove_btn").last().addClass("hide");
            }
        }
        function addSubCategory(i) {
            $("#plus_"+i).addClass("hide");
            i++;
            $("#sub-category-div").append("<div class='form-group row' id='sub_category_"+i+"'>" +
                "<label for='title_"+i+"' class='col-md-2 col-form-label text-md-right'>SubCategory Title</label>" +
                "<div class='col-md-3'>" +
                "<input id='title_"+i+"' type='text' class='form-control' name='title[]' required>" +
                "</div>" +
                "<label for='subtitle_"+i+"' class='col-md-2 col-form-label text-md-right'>SubCategory SubTitle</label>" +
                "<div class='col-md-3'>" +
                "<input id='subtitle_"+i+"' type='text' class='form-control' name='subtitle[]' required>" +
                "</div>" +
                "<div class='col-md-2'>" +
                "<button class='btn btn-primary plus_btn' id='plus_"+i+"' onclick='addSubCategory("+i+")'>+" +
                "</button>" +
                "<button class='btn btn-danger remove_btn' onclick='removeSubCategory("+i+")'>" +
                "-" +
                "</button>" +
                "</div>" +
                "</div>");
            $( ".remove_btn" ).removeClass( "hide" );
        }
    </script>
@endsection
