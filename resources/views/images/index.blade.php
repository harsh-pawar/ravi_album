@extends('layouts.app')

@section('content')
    <?php $i = 0; ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Images</div>

                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    <table width="100%" border="1">
                        <thead align="center">
                        <tr>
                            <th>Image</th>
                            <th>Index</th>
                            <th>Category</th>
                            <th>Title</th>
                            <th>SubTitle</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody align="center">
                        @foreach($images as $id=>$image)
                            <tr>
                                <td><img width="100px" height="100px" src="/public/image/{{ $image->image_name }}"></td>
                                <td>{{ $image->img_index }}</td>
                                <td>{{ $image->category->category_name }}</td>
                                <td>{{ $image->subcategory->title }}</td>
                                <td>{{ $image->subcategory->subtitle }}</td>
                                <td>
                                    <a href="/images/{{$image->id}}/edit">Edit</a> |
                                    <a href="#" onclick="deleteCategory({{$image->id}})">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <style>
        .hide {
            visibility: hidden;
        }
    </style>
@endsection

@section('js-script')
    <script>
        function deleteCategory(id) {
            if (confirm('Are you sure ?')) {
                window.location.replace("/images/"+id+"/delete");
            }
        }
    </script>
@endsection
