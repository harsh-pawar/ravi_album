@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Image</div>

                    <form method="post" action="/images/{{$image['id']}}/update">
                        @csrf
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Album Category</label>

                                <div class="col-md-8">
                                    <input id="category" type="text" class="form-control"
                                           value="{{ $image['category']['category_name'] }}"
                                           required disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Album Title</label>

                                <div class="col-md-8">
                                    <input id="category" type="text" class="form-control"
                                           value="{{ $image['subcategory']['title'] }}"
                                           required disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Album SubTitle</label>

                                <div class="col-md-8">
                                    <input id="category" type="text" class="form-control"
                                           value="{{ $image['subcategory']['subtitle'] }}"
                                           required disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Image Index</label>

                                <div class="col-md-8">
                                    <input id="category" type="number" class="form-control" name="img_index"
                                           value="{{ $image['img_index'] }}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">Image</label>

                                <div class="col-md-8">
                                    <img width="200px" height="200px" src="/public/image/{{ $image['image_name'] }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-10" align="right">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection