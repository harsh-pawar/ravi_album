@extends('layouts.app')

@section('content')
    <style>
        .thumb{
            margin: 10px 5px 0 0;
            width: 300px;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Images</div>

                    <form id="file-upload-form" class="uploader" action="/images" method="post" accept-charset="utf-8" enctype="multipart/form-data">

                        <div class="form-group row" style="margin-top: 10px;">
                            <label for="email" class="col-md-2 col-form-label text-md-right">Album Category</label>

                            <div class="col-md-8">
                                <select id="category_name_id" type="text" class="form-control" name="category_name_id"
                                         required autofocus>
                                    <option value="">Select Category</option>
                                    @foreach($categories as $id=>$category)
                                        <option value="{{$id}}">{{ $category }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">Album Title</label>

                            <div class="col-md-8">
                                <select id="category_title_id" type="text" class="form-control" name="category_title_id" required>
                                    <option value="">Select Title</option>
                                </select>
                            </div>
                        </div>
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">Select Images</label>
                            <div class="col-md-8">
                                <input type="file" class="form-control" id="file-input" name="image[]" multiple />
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                                <div id="thumb-output"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10" align="right">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-script')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script>

        function removeImage(img_index) {
            $(".img_div_"+img_index).remove();
        }

        $(document).ready(function(){
            //here
            $('#category_name_id').on('change', function() { //on file input change
                var category_id = $("#category_name_id").val();


                $.ajax({
                    url: '/getTitleAndSubtitle',
                    data: "category_id=" + category_id,
                    success: function (data) {
                        $.each(data, function (index,category) {
                            $("#category_title_id").append("<option value='"+category.id+"'>"+category.title+"--"+category.subtitle+"</option>")
                        });
                    }
                });

            });


            $('#file-input').on('change', function(){ //on file input change

                    var data = $(this)[0].files; //this file data
                    var i = 0;
                    $("#thumb-output").children().remove();

                    $.each(data, function(index, file){ //loop though each file
                        if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                            var fRead = new FileReader(); //new filereader
                            fRead.onload = (function(file){ //trigger function on successful read
                                return function(e) {
                                    var img = $('<img/>').attr("name",'image_here[]').attr("value",e.target.result).addClass('thumb img_div_'+i).attr('src', e.target.result); //create image element
                                    // var img = $('<input/>').attr("name",'image_here[]').attr("value",e.target.result).addClass('thumb img_div_'+i).attr('src', e.target.result); //create image element
                                    $('#thumb-output').append(img); //append image to output element
                                    var textfield = $('<input/>').addClass('form-control col-md-4 img_div_'+i).attr('type', "number").attr('placeholder', "Image Index").attr('name','img_index[]').attr('value',i+1).attr('style',"display:inline;"); //create image element
                                    $('#thumb-output').append(textfield); //append image to output element
                                    // var cross_sign = $('<a>X</a>').addClass('form-control btn btn-danger remove_btn col-md-1 img_div_'+i).attr('text',"X").attr('style',"display:inline; margin-left:10px;").attr("data-id",i).attr("onclick",'removeImage('+i+')');
                                    // $('#thumb-output').append(cross_sign);
                                    i++;
                                };
                            })(file);
                            fRead.readAsDataURL(file); //URL representing the file's data.
                        }
                    });

            });
        });

    </script>
@endsection
