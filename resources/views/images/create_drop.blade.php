@extends('layouts.app')

@section('content')
    {{-- <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script> --}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Images</div>

{{--                    <form id="file-upload-form" class="uploader" action="/images" method="post" accept-charset="utf-8" enctype="multipart/form-data">--}}

                        <div class="form-group row" style="margin-top: 10px;">
                            <label for="email" class="col-md-2 col-form-label text-md-right">Album Category</label>

                            <div class="col-md-8">
                                <select id="category_name_id" type="text" class="form-control" name="category_name_id"
                                        required autofocus>
                                    <option value="">Select Category</option>
                                    @foreach($categories as $id=>$category)
                                        <option value="{{$id}}">{{ $category }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">Album Title</label>

                            <div class="col-md-8">
                                <select id="category_title_id" type="text" class="form-control" name="category_title_id" required>
                                    <option value="">Select Title</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-group row">
                        @csrf
                        <label for="email" class="col-md-2 col-form-label text-md-right">Album Images</label>
                        <div class="col-md-8" style="margin-left: 20px;">

                            {!! Form::open([ 'route' => [ 'dropzone.store' ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}

                            <div>
                                <h3>Upload Multiple Image By Click On Box</h3>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-10" align="right">
                            <button onclick="mapImages()" class="btn btn-primary">
                                Submit
                            </button>
                        </div>
                    </div>
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-script')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        function mapImages() {

            var category_id = $("#category_name_id").val();
            var title_id = $("#category_title_id").val();

            if(category_id == "" || title_id == ""){
                alert("Category or Title should not be blank");
                return;
            }

            $.ajax({
                url: '/mapImages',
                data: "category_id=" + category_id+"&title_id="+title_id,
                success: function (data) {
                    alert("Image map successfully.");
                    location.reload(true);
                }
            });

        }

        var fileList = new Array;
        var i = 0;
        var file_up_names = [];
        var file_server_names = [];
        Dropzone.options.imageUpload = {

            maxFilesize: 5,
            addRemoveLinks: true,
            parallelUploads: 10,
            acceptedFiles: ".jpeg,.jpg,.png,.gif,.webp",
            success:function(file,serverFileName) {
                file_up_names.push(file.name);
                file_server_names.push(serverFileName);
            },
            init: function () {

            },
            removedfile: function(file) {
                x = confirm('Do you want to delete?');
                if(!x)  return false;
                for(var i=0;i<file_up_names.length;++i){

                    if(file_up_names[i]==file.name) {

                        $.ajax({
                            url: '/images/delete',
                            data: "file_name=" + file_server_names[i],
                            success: function (data) {
                                if(data == "success") {
                                    file.previewElement.remove();
                                } else {
                                    alert(data);
                                }
                            }
                        });
                    }
                }

            }

        };

        $(document).ready(function(){

            $('#category_name_id').on('change', function() { //on file input change
                var category_id = $("#category_name_id").val();


                $.ajax({
                    url: '/getTitleAndSubtitle',
                    data: "category_id=" + category_id,
                    success: function (data) {
                        $.each(data, function (index,category) {
                            $("#category_title_id").append("<option value='"+category.id+"'>"+category.title+"--"+category.subtitle+"</option>")
                        });
                    }
                });

            });


            $('#file-input').on('change', function(){ //on file input change

                var data = $(this)[0].files; //this file data
                var i = 0;
                $("#thumb-output").children().remove();

                $.each(data, function(index, file){ //loop though each file
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                        var fRead = new FileReader(); //new filereader
                        fRead.onload = (function(file){ //trigger function on successful read
                            return function(e) {
                                var img = $('<img/>').attr("name",'image_here[]').attr("value",e.target.result).addClass('thumb img_div_'+i).attr('src', e.target.result); //create image element
                                // var img = $('<input/>').attr("name",'image_here[]').attr("value",e.target.result).addClass('thumb img_div_'+i).attr('src', e.target.result); //create image element
                                $('#thumb-output').append(img); //append image to output element
                                var textfield = $('<input/>').addClass('form-control col-md-4 img_div_'+i).attr('type', "number").attr('placeholder', "Image Index").attr('name','img_index[]').attr('value',i+1).attr('style',"display:inline;"); //create image element
                                $('#thumb-output').append(textfield); //append image to output element
                                // var cross_sign = $('<a>X</a>').addClass('form-control btn btn-danger remove_btn col-md-1 img_div_'+i).attr('text',"X").attr('style',"display:inline; margin-left:10px;").attr("data-id",i).attr("onclick",'removeImage('+i+')');
                                // $('#thumb-output').append(cross_sign);
                                i++;
                            };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.
                    }
                });

            });
        });

    </script>

@endsection
