<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/category', "CategoryController@index");
    Route::get('/category/create', "CategoryController@create");
    Route::post('/category', "CategoryController@store");
    Route::post('/category/{id}/update', "CategoryController@update");
    Route::get('/category/{id}/edit', "CategoryController@edit");
    Route::get('/category/{id}/delete', "CategoryController@destroy");

    Route::get('/getTitleAndSubtitle', "SubCategoryController@getTitleAndSubtitle");

    Route::get('/subcategory', "SubCategoryController@index");
    Route::post('/subcategory', "SubCategoryController@store");
    Route::get('/subcategory/create', "SubCategoryController@create");
    Route::post('/subcategory/{id}/update', "SubCategoryController@update");
    Route::get('/subcategory/{id}/edit', "SubCategoryController@edit");
    Route::get('/subcategory/{id}/delete', "SubCategoryController@destroy");

    Route::get('/images', "ImageController@index");
    Route::get('/images/create', "ImageController@create");
    Route::post('/images', "ImageController@store");
    Route::post('/images/store', ['as' => 'dropzone.store', 'uses' => "ImageController@store"]);
    Route::get('/images/delete', ['as' => 'dropzone.delete', 'uses' => "ImageController@delete"]);
    Route::get('/mapImages', ['as' => 'dropzone.imageMap', 'uses' => "ImageController@mapImages"]);
    Route::get('/images/{id}/delete', "ImageController@destroy");
    Route::get('/images/{id}/edit', "ImageController@edit");
    Route::post('/images/{id}/update', ['as' => 'images.list', 'uses' => "ImageController@update"]);

});
