<?php

namespace App\Http\Controllers;

use App\Category;
use App\ImageMapper;
use App\SubCategory;
use Illuminate\Http\Request;

class ImageController extends Controller
{

    public function index()
    {
        $images = ImageMapper::with('category', 'subcategory')->orderBy('img_index','ASC')->get();

        return view('images.index')->with("images",$images);
    }

    public function create() {

        $categories = Category::pluck('category_name','id');

//        return view('images.create')->with('categories',$categories);
        return view('images.create_drop')->with('categories',$categories);

    }

    public function store(Request $request) {

        $insert = [];
        $data = $request->all();

        $files = $request->file('file');
        $response = [];
        $destinationPath = 'public/image/'; // upload path

//        foreach ($files as $file) {
            $profileImage = microtime() . "." . $files->getClientOriginalExtension();
//        $profileImage = $files->getClientOriginalName(); //. "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
//            array_push($response,$profileImage);
//        }
            $image = new ImageMapper();
            $image->image_name = $profileImage;
            $image->save();

        return $profileImage;
//        return $files->getClientOriginalName();
    }

    public function delete(Request $request) {

        $data = $request->all();
        $file_name = $data['file_name'];
        $destinationPath = 'public/image/'; // upload path
        if($file_name != "") {
            if(unlink("$destinationPath".$file_name)){
                $image = ImageMapper::where('image_name',$file_name)->first();
                if($image) {
                    $image->delete();
                }
                return "success";
            } else {
                return "File not found";
            }
        } else {
            return "File name not found";
        }

    }

    public function mapImages(Request $request) {

        $data = $request->all();
        $category_id = $data['category_id'];
        $title_id = $data['title_id'];

        $images_list = ImageMapper::whereNull("category_id")->pluck('id');
        if(isset($images_list) && sizeof($images_list) > 0) {
            $max_index = ImageMapper::where("category_id",$category_id)->where('sub_category_id',$title_id)
                ->orderBy('id','DESC')->pluck("img_index")->toArray();
            $index = 0;
            if(sizeof($max_index) > 0) {
                $index = $max_index[0] + 1;
            } else {
                $index = 1;
            }
            foreach ($images_list as $image_id) {

                $image_obj = ImageMapper::find($image_id);
                $image_obj->img_index = $index++;
                $image_obj->category_id = $category_id;
                $image_obj->sub_category_id = $title_id;
                $image_obj->save();

            }
        }

        return "success";
    }

    public function destroy(Request $request, $id) {

        $destinationPath = 'public/image/'; // upload path
            $image = ImageMapper::find($id);
            if(isset($image) && !empty($image)) {
                $file_name = $image->image_name;
                if (unlink("$destinationPath" . $file_name)) {
                    $image->delete();
                }
                return back()->with("success","Image deleted successfully.");
            } else {
                return back()->with("error","Error in image delete.");
            }
    }

    public function edit(Request $request, $id) {

        $images = ImageMapper::with('category', 'subcategory')->where('id',$id)->first()->toArray();

        return view("images.edit")->with("image",$images);

    }

    public function update(Request $request, $id) {

        $request = $request->all();
        $images = ImageMapper::find($id);

        $images->img_index = $request['img_index'];
        $images->save();

        return redirect('images')->with("success","Image update successfully.");

    }

}
