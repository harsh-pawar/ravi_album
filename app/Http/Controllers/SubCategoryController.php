<?php

namespace App\Http\Controllers;

use App\Category;
use App\ImageMapper;
use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        $title = SubCategory::with('category')->get();
        return view('subcategory.index')->with("title_array",$title);
    }

    public function create() {

        $category_list = Category::all()->pluck("category_name",'id');
        return view('subcategory.create')->with('categories',$category_list);

    }

    public function store(Request $request) {

        $data = $request->all();

        $subcategory = SubCategory::create($data);

        return redirect('/subcategory')->with('status','SubCategory added successfully.');

    }

    public function edit($id) {

        if($id != "") {
            $subcategory = SubCategory::with('category')->where('id',$id)->get();
            if(isset($subcategory) && !empty($subcategory)) {
                return view('subcategory.edit')->with('subcategory',$subcategory[0]);
            } else {
                return back()->with('error', 'SubCategory not found');
            }

        } else {
            return back()->with('error','ID not found.');
        }
    }

    public function update(Request $request, $id) {

        if($id != "") {
            $subcategory = SubCategory::find($id);
            $temp_sub_priority = 0;
            if(isset($subcategory) && !empty($subcategory)) {
                $subcategory->title = $request['title'];
                $subcategory->subtitle = $request['subtitle'];
                $temp_sub_priority = $subcategory->sub_priority;
                $subcategory->sub_priority = $request['sub_priority'];
                $subcategory->status = $request['status'];
//                print_r(json_encode($subcategory));exit;
                $subcategory_change = SubCategory::where('sub_priority', $request['sub_priority'])->first();

                if(isset($subcategory_change) && !empty($subcategory_change)){
                    $subcategory_change->sub_priority = $temp_sub_priority;
                    $subcategory_change->save();
                }

                $subcategory->save();

                return redirect('/subcategory')->with('status','SubCategory updated successfully.');
            } else {
                return back()->with('error', 'SubCategory not found');
            }

        } else {
            return back()->with('error','ID not found.');
        }

    }

    public function destroy(Request $request, $id) {

        $sub_category = SubCategory::find($id);

        if(isset($sub_category) && !empty($sub_category)){

            $sub_category->delete();
            $destinationPath = 'public/image/'; // upload path

            $image_list = ImageMapper::where('sub_category_id',$id)->pluck('id');

            foreach ($image_list as $image_id) {

                $image = ImageMapper::find($image_id);
                if(isset($image) && !empty($image)) {
                    $file_name = $image->image_name;
                    if (unlink("$destinationPath" . $file_name)) {
                        $image->delete();
                    }
                }

            }

            return redirect('/category')->with('status','Category deleted successfully.');
        } else {
            return back()->with('error', 'Category not found');
        }

    }

    public function getTitleAndSubtitle(Request $request) {

        $data = $request->all();
        $category_id = $data['category_id'];

        $titles = SubCategory::where('album_category_id',$category_id)->get();

        return $titles;

    }

}
