<?php

namespace App\Http\Controllers;

use App\Category;
use App\ImageMapper;
use Illuminate\Http\Request;
use stdClass;

class ApiController extends Controller
{

    public function category() {

//        $categpory = Category::with("subCategories.images" )->get();
        $categories = Category::with(
            ["subCategories.images"=> function ($query) {
            $query->orderBy('img_index', 'asc');
        },"subCategories"=> function ($query) {
                $query->orderBy('sub_priority', 'asc');
                $query->where('status', 'active');
            }]
        )->orderBy('priority','asc')->where('status','active')->get();

        $response = new STDClass();
        $response->status = 1; 
        $response->message = "Category list retrieved successfully";
        $response->data = [];

        foreach ($categories as $category) {

            $data = new STDClass();
            $data->id = $category->id;
            $data->category_name = $category->category_name;
            $data->status = $category->status;
            $data->deleted_at = $category->deleted_at;
            $data->created_at = $category->created_at;
            $data->updated_at = $category->updated_at;

            $data->subCategory = json_decode(json_encode($category))->sub_categories;
                foreach ($data->subCategory as $sub_category) {
                    $sub_category->photos_pic_original = "";
                    $sub_category->imageOne = "";
                    $sub_category->imageTwo = "";
                    $sub_category->imageThree = "";
                    $sub_category->imageFour = "";
                        foreach ($sub_category->images as $index => $image) {
                            if ($index == 0) {
                                $sub_category->photos_pic_original = env("APP_URL").$image->image_name;
                                $sub_category->imageOne = env("APP_URL").$image->image_name;
                            } else if ($index == 1) {
                                $sub_category->imageTwo = env("APP_URL").$image->image_name;
                            } else if ($index == 2) {
                                $sub_category->imageThree = env("APP_URL").$image->image_name;
                            } else if ($index == 3) {
                                $sub_category->imageFour = env("APP_URL").$image->image_name;
                            }
                        }
                    unset($sub_category->images);
                }
            array_push($response->data,$data);
        }
       return json_encode($response);
    }

    public function images(Request $request) {
        $category_id = $request->category_id;
        $sub_category_id = $request->sub_category_id;

        $images = ImageMapper::where('category_id',$category_id)
                            ->where('sub_category_id',$sub_category_id)->pluck('image_name','id');

        $response = new STDClass();
        $response->status = 1; 
        $response->message = "Image list retrieved successfully";
        $response->data = [];

        foreach ($images as $id=>$image) {
            $data = new STDClass();
            $data->id = $id;
            $data->photos_pic_original = env("APP_URL").$image;

            array_push($response->data,$data);
        }
        
        return json_encode($response);
    }

}
