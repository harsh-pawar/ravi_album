<?php

namespace App\Http\Controllers;

use App\Category;
use App\ImageMapper;
use App\SubCategory;
use foo\bar;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = Category::all();
        return view('category.index')->with("title_array",$title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $album_category = Category::create($data);
        $data['album_category_id'] = $album_category->id;
        $priority = 1;

        foreach ($data['title'] as $index=>$title) {

            SubCategory::insert(['album_category_id'=>$album_category->id, "title"=>$title, "subtitle"=>$data['subtitle'][$index], 'sub_priority'=>$priority++]);

        }

        return redirect('/category')->with("status","Category added successfully");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category, $id)
    {
        if($id != "") {
            $category = Category::find($id);
            if(isset($category) && !empty($category)) {
                return view('category.edit')->with('category',$category);
            } else {
                return back()->with('error', 'Category not found');
            }

        } else {
            return back()->with('error','ID not found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id != "") {
            $category = Category::find($id);
            if(isset($category) && !empty($category)) {
                $category->category_name = $request['category_name'];
                $category->status = $request['status'];
                $temp_priority = $category->priority;
                $category->priority = $request['priority'];

                $category_change = Category::where('priority', $request['priority'])->first();

                if(isset($category_change) && !empty($category_change)){
                    $category_change->priority = $temp_priority;
                    $category_change->save();
                }

                $category->save();
                return redirect('/category')->with('status','Category updated successfully.');
            } else {
                return back()->with('error', 'Category not found');
            }

        } else {
            return back()->with('error','ID not found.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, $id)
    {
        $destinationPath = 'public/image/'; // upload path
        if($id != "") {
            $category = Category::find($id);
            if(isset($category) && !empty($category)) {
                $category->delete();
                $subcategories = SubCategory::where('album_category_id',$id)->pluck('id');
                if(isset($subcategories) && !empty($subcategories)) {
                    foreach ($subcategories as $subcategory){
                        if($subcategory > 0 ){
                           $subcategory =  SubCategory::find($subcategory);
                           $subcategory->delete();
                        }
                    }
                }
                $image_list = ImageMapper::where('category_id',$id)->pluck('id');

                foreach ($image_list as $image_id) {

                    $image = ImageMapper::find($image_id);
                    if(isset($image) && !empty($image)) {
                        $file_name = $image->image_name;
                        if (unlink("$destinationPath" . $file_name)) {
                            $image->delete();
                        }
                    }

                }
                return redirect('/category')->with('status','Category deleted successfully.');
            } else {
                return back()->with('error', 'Category not found');
            }

        } else {
            return back()->with('error','ID not found.');
        }
    }
}
