<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    use SoftDeletes;

    protected $table = "album_category";
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'category_name', 'status', 'priority'
    ];

    public function subCategories() {

        return $this->hasMany('App\SubCategory','album_category_id');

    }

}
